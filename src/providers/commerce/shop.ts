import {Injectable} from '@angular/core';
import {Http,Headers,RequestOptions} from '@angular/http';
import {Storage} from '@ionic/storage';
import 'rxjs/add/operator/toPromise';
import *  as AppConfig from '../config';
// import { resolve } from 'path';

@Injectable()
export class ShopProvider {

  private cfg: any;
  idToken: string;
  refreshSubscription: any;
  
  constructor(
    private storage: Storage,
    private http: Http) 
    {
    this.cfg = AppConfig.cfg;
    this.storage.get('id_token').then(token => {this.idToken = token; });
    }

    getCategories(id:any){
        return this.getAPI(this.cfg.apiUrl + this.cfg.cat.top + id).then(data=>{
            return data;
        });
    }
    getPopProducts(token:any){
        return this.storage.get('active').then(active=>{
            if (active){
                return this.postAPI(this.cfg.apiUrl + this.cfg.prod.popular,{'popular':'true'},token);
            } else {
                return false;
            }
        })
    }
    getCatProducts(token:any,id:any){
        return this.storage.get('active').then(active=>{
            if (active){
                return this.postAPI(this.cfg.apiUrl + this.cfg.cat.product,{'cat_id':id},token);
            } else {
                return false;
            }
        })
    }
    getProduct(token:any,id:any){
        return this.storage.get('active').then(active=>{
            if (active){
                return this.postAPI(this.cfg.apiUrl + this.cfg.product,{'product_id':id},token);
            } else {
                return false;
            }
        })
    }
    placeOrder(token:any,order:any){
        return this.storage.get('active').then(active=>{
            if (active){
                return this.postAPI(this.cfg.apiUrl + this.cfg.order,order,token);
            } else {
                return false;
            }
        })
    }
    
    allProducts(token:any,limit,page){
        return this.postAPI(this.cfg.apiUrl+this.cfg.products,{'limit':limit,'page':page},token).then(data=>{
            return data;
        });
    }
    search(token:any,limit,page,keyword){
        return this.postAPI(this.cfg.apiUrl+this.cfg.search,{'limit':limit,'page':page,'keyword':keyword},token).then(data=>{
            return data;
        });
    }
    getHistory(token){
        return this.postAPI(this.cfg.apiUrl+this.cfg.history,{history:'true'},token);
    }
    getAPI(address:string){
        return this.http.get(address).toPromise().then((data)=>{
            let res = data.json();
            if (res.status=='success'){
                return data.json();
            } else {
                return [];
            }
       })
    }

    postAPI(address:string, data:any,token:any){
        let headers = new Headers();
        headers.append('Content-Type','application/json');
        headers.append('X-Auth-Token',token);
        let reqOpts = new RequestOptions({headers:headers});
        return this.http.post(address,data,reqOpts).toPromise().then((data)=>{
            let res = data.json();
            return res;
        })
    }
}