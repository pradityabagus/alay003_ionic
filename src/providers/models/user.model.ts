export class UserModel {
 
    public name: string;
    public address: string;
    public phone: string;
    public password: string;
    public confirm_password?: string;
     
  }
  