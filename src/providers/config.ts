export let cfg = {
    apiUrl: 'https://luckmasadmin.com/api/v1',
    img: 'https://luckmasadmin.com/', 
    tokenName: 'token',
    user: {
      register: '/register',
      login: '/login',
      refresh:'/refresh',
      active:'/active/',
      logout:'/logout/',
      update:'/updateprofile',
      changepass:'/changepassword',
    },
    cat: {
      top: '/category/top/',
      product: '/category/product',
    },
    prod: {
      popular:'/popularproduct',
    },
    product: '/product',
    order: '/placeorder',
    history: '/orderhistory',
    products: '/products',
    search: '/search',
  };