import { Component, ViewChild } from '@angular/core';
import { Nav, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import {Storage} from '@ionic/storage';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ShopProvider } from '../providers/commerce/shop';
import { AuthProvider } from '../providers/auth/auth';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = 'HomePage';
  refreshSubscription: any;

  pages: Array<{title: string, component: any, icon:string}>;
  categories:any[];

  constructor(public platform: Platform, public statusBar: StatusBar, public storage: Storage, public shopService: ShopProvider,
    public splashScreen: SplashScreen,public authService:AuthProvider) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: 'HomePage', icon:'home' },      
      { title: 'Edit Profil', component: 'ProfilePage', icon:'create' },
      { title: 'Keranjang Belanja', component: 'CartPage', icon:'cart' },
      { title: 'Hubungi Admin', component: 'KontakPage', icon:'chatbubbles' },
      { title: 'Status Order', component: 'StatusorderPage', icon:'list' }           
    ];

    this.getCategories();

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      // console.log('appready');
      // this.updateActive();
    });
  }

  updateActive(){
    this.storage.get('token').then(token => {
      if (token!==null){
        console.log(token);
        this.authService.activeUser(token);
      };
    });
  }
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  
  openAllProducts(){
    this.nav.setRoot('AllProductPage');
  }

  goToCategory(category){
     this.nav.setRoot('CategoryPage',{category:category})
  }

  getCategories(){
    this.shopService.getCategories(0).then(data=>{
      let temp = data;
      this.categories = temp.category;
    })
  }
  logout() {
    this.authService.logout();
    this.nav.setRoot('LoginPage');
  }
  public unscheduleRefresh() {
    // Unsubscribe fromt the refresh
    if (this.refreshSubscription) {
      this.refreshSubscription.unsubscribe();
    }
  }
}
