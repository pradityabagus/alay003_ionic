
import { config } from './../config/app.config';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, LOCALE_ID, NgModule } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeId from '@angular/common/locales/id';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';
import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import * as firebase from 'firebase';
import { ProductsProvider } from '../providers/products/products';
import { CategoryProvider } from '../providers/category/category';
import { CartProvider } from '../providers/cart/cart';
import { AuthProvider } from '../providers/auth/auth';
import { ShopProvider } from '../providers/commerce/shop';
import { OrderProvider } from '../providers/order/order';
import { Http,HttpModule } from '@angular/http'; 
import {AuthHttp, AuthConfig,JwtHelper} from 'angular2-jwt';
import {Storage} from '@ionic/storage';
import { IonicImageViewerModule } from 'ionic-img-viewer';


let storage = new Storage({});

firebase.initializeApp(config.firebasConfig);
export function getAuthHttp(http) {
  return new AuthHttp(new AuthConfig({
    noJwtError: true,
    globalHeaders: [{'Accept': 'application/json'}],
    tokenGetter: (() => storage.get('id_token')),
  }), http);
}
registerLocaleData(localeId);

@NgModule({
  declarations: [
    MyApp,
    
  ],
  imports: [
    BrowserModule, HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    IonicImageViewerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    JwtHelper,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {
      provide: AuthHttp,
      useFactory: getAuthHttp,
      deps: [Http]
    },
    { provide: LOCALE_ID, useValue: 'id' },
    ProductsProvider,
    CategoryProvider,
    NativePageTransitions,
    CartProvider,
    AuthProvider,
    OrderProvider,
    ShopProvider
  ]
})
export class AppModule {}
