import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ToastController,
} from "ionic-angular";
import { Storage } from "@ionic/storage";
import { ProtectedPage } from '../protected-page/protected-page';
import { CartProvider } from "../../providers/cart/cart";
import { AuthProvider } from "../../providers/auth/auth";
import { ShopProvider } from "../../providers/commerce/shop"

@IonicPage()
@Component({
  selector: "page-checkout",
  templateUrl: "checkout.html"
})
export class CheckoutPage extends ProtectedPage{
  cartItems: any[] = [];
  productAmt: number = 0;
  totalAmount: number = 0;
  shippingFee: number = 0;
  customerName: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage:Storage,
    private cartService: CartProvider,
    private loadingCtrl: LoadingController,
    public toastCtrl:ToastController,
    public authService: AuthProvider,
    public shopService: ShopProvider,
  ) {
    super(navCtrl,navParams,storage,authService);
    this.loadCartItems();
  }

  loadCartItems() {
    let loader = this.loadingCtrl.create({
      content: "Mohon tunggu.."
    });
    loader.present();
    this.cartService
      .getCartItems()
      .then(val => {
        this.cartItems = val;

        if (this.cartItems.length > 0) {
          this.cartItems.forEach((v, indx) => {
            this.productAmt += parseInt(v.totalPrice);
          });

          this.totalAmount = this.productAmt + this.shippingFee;
        }
        loader.dismiss();
      })
      .catch(err => {});
  }

  
  placeOrder() {
    let loader1 = this.loadingCtrl.create({
      content: "Menyelesaikan pemesanan..."
    });
    let orderObj = {
      amount: this.totalAmount,
      orders: this.cartItems
    };
    this.storage.get('token').then(token=>{
      loader1.present();
      this.shopService.placeOrder(token,orderObj).then(data => {
        loader1.dismiss();
        this.storage.remove('cartItems');
        this.presentToast(data.message);
        this.navCtrl.setRoot('StatusorderPage');
      }, error=>{
        loader1.dismiss();
      });
    });
    
  }
  back(){
    this.navCtrl.setRoot('CartPage');
  }
  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      showCloseButton: true,
      duration: 4000,
      closeButtonText: 'tutup'
    });

    toast.present();
  }
  goBackHome(){
    this.navCtrl.setRoot('AllProductPage');
  }
}
