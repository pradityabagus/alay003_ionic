import { Component,ViewChild } from '@angular/core';
import { IonicPage, LoadingController,NavController, NavParams } from 'ionic-angular';
import { ProtectedPage } from '../protected-page/protected-page';
import { ShopProvider } from '../../providers/commerce/shop';
import { AuthProvider } from '../../providers/auth/auth';
import { Storage } from '@ionic/storage';
import *  as AppConfig from '../../providers/config';


@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage extends ProtectedPage{
  public isSearchBarOpened = false;
  @ViewChild('searchbar') searchbar : any;
  page = 1;
  perPage = 20;
  totalData = 0;
  totalPage = 0;
  products:any[];
  productRows:any;
  cfg:any;
  showInf = true;
  keyword:string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl:LoadingController,
    public storage:Storage,public shopService:ShopProvider,public authService:AuthProvider) {
    super(navCtrl,navParams,storage,authService);
    this.cfg=AppConfig.cfg;
    this.keyword = this.navParams.get("keyword");
  }

  ionViewDidLoad() {
    let loader = this.loadingCtrl.create({
      content: 'Memuat produk...'
    });
    loader.present();
    this.storage.get('token').then(token=>{
      this.shopService.search(token,this.perPage,this.page,this.keyword).then(data=>{
        this.products = data.data.products;
        this.totalPage= data.data.paginator.total_pages;
        this.productRows = Array.from(Array(Math.ceil(this.products.length/2)).keys());
        this.products.forEach(product => {
          product.thumb=this.cfg.img + product.thumb;
        });
        this.showInf= (this.page<this.totalPage)?true:false;
        loader.dismiss();
        this.totalData= data.data.paginator.total_count;
      }, error=>{
        loader.dismiss();
      });
    })
  }
  searchBtn(){
    this.isSearchBarOpened=true;
    setTimeout(() => {
      this.searchbar.setFocus();
    }, 100);
  }
  
  closeSearch(){
    this.isSearchBarOpened=false;
  }

  onSearch(event){
    this.navCtrl.setRoot("SearchPage",{keyword:event.target.value});
  }
  
  showDetails(product){
    this.navCtrl.push("SinglePage",{product:product});    
  }

}
