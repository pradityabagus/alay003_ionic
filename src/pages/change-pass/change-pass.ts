import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ProtectedPage } from '../protected-page/protected-page';
import { AuthProvider } from '../../providers/auth/auth';

/**
 * Generated class for the ChangePassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-change-pass',
  templateUrl: 'change-pass.html',
})
export class ChangePassPage extends ProtectedPage{
  passwordType: string[] = ['password','password','password'];
  passwordIcon: string[] = ['eye-off','eye-off','eye-off'];
  old_pass:string;
  new_pass:string;
  new_pass_confirm:string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage:Storage, public authService:AuthProvider,
    private loadingCtrl:LoadingController, private alertCtrl:AlertController,
  ) {
    super(navCtrl,navParams,storage,authService);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePassPage');
  }

  hideShowPassword(idx) {
    this.passwordType[idx] = this.passwordType[idx] === 'text' ? 'password' : 'text';
    this.passwordIcon[idx] = this.passwordIcon[idx] === 'eye-off' ? 'eye' : 'eye-off';
  }
  
  savePassword(){
    if (!this.old_pass || !this.new_pass || !this.new_pass_confirm){
      this.presentAlert('Semua kolom harus diisi.');
      return false;
    }
    if (this.new_pass != this.new_pass_confirm){
      this.presentAlert('password baru dan konfirmasi password tidak sama');
      return false;
    }
    this.storage.get('token').then(token=>{
      let loader = this.loadingCtrl.create({
        content: 'Mengambil Data..'
      });
      loader.present();
      var dataPass ={
        old_pass : this.old_pass,
        new_pass : this.new_pass,
        new_pass_confirm : this.new_pass_confirm,
      }
      this.authService.changePass(dataPass,token).then(data=>{
        this.presentAlert(data.message)
        if (data.status_code==200){
          this.old_pass = '';
          this.new_pass = '';
          this.new_pass_confirm = '';
          }
        if (data.status_code==266){
          this.authService.logout();
          this.navCtrl.setRoot('LoginPage');
        }
        loader.dismiss();
      },error=>{
        //this.presentAlert(error.message);
        //this.authService.logout();
        //this.navCtrl.setRoot('LoginPage');
        loader.dismiss()
      })
    })
  }

  presentAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'Luck Mas',
      subTitle: message,
      buttons: ['Close']
    });
    alert.present();
  }
}
