import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WaitpagePage } from './waitpage';

@NgModule({
  declarations: [
    WaitpagePage,
  ],
  imports: [
    IonicPageModule.forChild(WaitpagePage),
  ],
})
export class WaitpagePageModule {}
