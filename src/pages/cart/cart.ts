import { Component } from "@angular/core";
import {IonicPage, NavController, NavParams, LoadingController} from "ionic-angular";
import { ProtectedPage } from '../protected-page/protected-page';
import { Storage } from '@ionic/storage';
import { AuthProvider } from '../../providers/auth/auth';
import { CartProvider } from "../../providers/cart/cart";

@IonicPage()
@Component({
  selector: "page-cart",
  templateUrl: "cart.html"
})
export class CartPage extends ProtectedPage{
  cartItems: any[] = [];
  totalAmount: number = 0;
  isCartItemLoaded: boolean = false;
  isEmptyCart: boolean = true;
  constructor(
    public navCtrl: NavController, public navParams: NavParams, private cartService: CartProvider, private loadingCtrl: LoadingController,
    public authService:AuthProvider, public storage:Storage,
  ) {
    super(navCtrl,navParams,storage,authService);
  }

  ionViewDidLoad() {
    this.loadCartItems();
  }

  loadCartItems() {
    let loader = this.loadingCtrl.create({
      content: "Mohon tunggu.."
    });
    loader.present();
    this.cartService
      .getCartItems()
      .then(val => {
        this.cartItems = val;
        this.totalAmount = 0;
        if (this.cartItems.length > 0) {
          this.cartItems.forEach((v, indx) => {
            this.totalAmount += parseInt(v.totalPrice);
          });
          this.isEmptyCart = false;
           loader.dismiss();
          }else{
            this.isEmptyCart = true;
            loader.dismiss();
          }
          
          this.isCartItemLoaded = true;
          // loader.dismiss();
        })
        .catch(err => {loader.dismiss();});
          
  }

  checkOut() {
      this.navCtrl.push("CheckoutPage");
  }

  removeItem(itm) {
    this.cartService.removeFromCart(itm).then((data) => {
      this.loadCartItems();
    });
  }
  showDetails(product){
    this.navCtrl.push("SinglePage",{product:product});    
  }
  
  goBackHome(){
    this.navCtrl.setRoot('AllProductPage');
  }
}
