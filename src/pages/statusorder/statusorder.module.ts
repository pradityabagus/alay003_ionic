import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StatusorderPage } from './statusorder';

@NgModule({
  declarations: [
    StatusorderPage,
  ],
  imports: [
    IonicPageModule.forChild(StatusorderPage),
  ],
})
export class StatusorderPageModule {}
