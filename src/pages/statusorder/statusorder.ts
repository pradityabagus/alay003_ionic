import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ProtectedPage } from '../protected-page/protected-page';
import { AuthProvider } from '../../providers/auth/auth';
import { ShopProvider } from '../../providers/commerce/shop';
@IonicPage()
@Component({
  selector: 'page-statusorder',
  templateUrl: 'statusorder.html',
})
export class StatusorderPage extends ProtectedPage {
  text:string;
  orders: any[];
  debts: any[];
  orderTitle: string;
  debtTitle: string;
  debtTotal: string;
  orderTotal: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public shopService: ShopProvider, private loadingCtrl: LoadingController, public authService: AuthProvider) {
    super(navCtrl, navParams, storage, authService);
  }

  ionViewDidLoad() {
    let loader = this.loadingCtrl.create({
      content: 'Memproses Data..'
    });
    loader.present();
    this.storage.get('token').then(token => {
      this.shopService.getHistory(token).then(esp => {
        var data = esp;
        //console.log(data);
        this.orderTitle = 'Order Baru';
        this.debtTitle = 'Hutang';
        this.orders = data.orders;
        this.debts = data.debts;
        this.debtTotal = data.totaldebts;
        this.orderTotal = data.totalorders;
        loader.dismiss()
      }, error=>{
        loader.dismiss();
      })
    })
    this.storage.get('fullname').then((result) => {
      this.myfunc(result)
    });

  }
  myfunc(str) {
    console.log(str);
    this.text = str;
  }
}
