import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, MenuController, AlertController} from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {AuthProvider} from '../../providers/auth/auth';

import {UserModel} from '../../providers/models/user.model';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  phone:any;
  password:any;
  public user: UserModel;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public storage: Storage,
    private alertCtrl: AlertController,
    public authService: AuthProvider) {
    this.menuCtrl.enable(false, 'menuMain');
    
  }

  ionViewDidLoad() {
    //hide menu when on the login page, regardless of the screen resolution
    // this.menuCtrl.enable(false);
  }

  login() {
    //use this.loginData.value to authenticate the user
    let regData = {
      phone:this.phone,
      password:this.password
    }
    this.authService.login(regData)
      .then((data) => {
        let status:any=data;
        if (status.status=="error"){
          this.presentAlert(status.message);
        } else {
          this.presentAlert(status.message);
          this.redirectToHome();
        }
      })
      .catch(e => console.log("login error", e));
  }

  redirectToHome() {
    this.navCtrl.setRoot('HomePage');
    this.menuCtrl.enable(true);
  }

  /**
   * Opens a paage
   * 
   * @param page string Page name
   */
  openPage(page: string) {
    this.navCtrl.setRoot(page);
  }
  showRegisterPage() {
    this.navCtrl.setRoot("RegisterPage");
  }

  presentAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'Luck Mas',
      subTitle: message,
      buttons: ['Close']
    });
    alert.present();
  }
}
