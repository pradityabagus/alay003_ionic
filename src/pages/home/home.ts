import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events } from 'ionic-angular';
import {Storage} from '@ionic/storage';
import { ProductsProvider } from '../../providers/products/products';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { AuthProvider } from '../../providers/auth/auth';
import { ShopProvider } from '../../providers/commerce/shop';
import *  as AppConfig from '../../providers/config';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage{
  public isSearchBarOpened = false;
  @ViewChild('searchbar') searchbar : any;
  private cfg: any;
  promoSliders: any[];
  products: any[];
  productRows:any;
  promoImagesLoaded:boolean=false;
  showBtn: boolean = false;
  deferredPrompt;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private productService: ProductsProvider,public authService:AuthProvider,
    public storage:Storage,public shopService:ShopProvider,
    private loadingCtrl: LoadingController,
    private events: Events,private nativePageTransitions: NativePageTransitions) {
    this.cfg=AppConfig.cfg;
  }
  
  ionViewCanEnter() {

    this.storage.get('token').then(token => {
      if (token === null) {
        this.navCtrl.setRoot('LoginPage');
        return false;
      } else {
        this.checkActive(token);
      }
    });
    
  }
  checkActive(token:any){
    this.authService.activeUser(token).then(trus=>{
      if (trus==0 || trus===null){
        this.navCtrl.setRoot('WaitpagePage');
      }
      if (trus==1){
        this.loadPromo();
        this.loadProducts();
      }
      if (trus==266){
        this.authService.logout();
        this.navCtrl.setRoot('LoginPage');
      }
    })
  }

  ionViewDidLeave() {
    this.events.unsubscribe('promoLoaded');
  }

  ionViewWillLeave() {
    let options: NativeTransitionOptions = {
       direction: 'up',
       duration: 500,
       slowdownfactor: 3,
       slidePixels: 20,
       iosdelay: 100,
       androiddelay: 150,
       fixedPixelsTop: 0,
       fixedPixelsBottom: 60
      };
   
    this.nativePageTransitions.slide(options)
      .then(()=>{

      })
      .catch((err)=>{

      });
   
   }

   

  ionViewDidLoad() {
    
  }

  ionViewWillEnter(){
    window.addEventListener('beforeinstallprompt', (e) => {
      // Prevent Chrome 67 and earlier from automatically showing the prompt
      e.preventDefault();
      // Stash the event so it can be triggered later on the button event.
      this.deferredPrompt = e;
       
    // Update UI by showing a button to notify the user they can add to home screen
      this.showBtn = true;
    });
     
    //button click event to show the promt
             
    window.addEventListener('appinstalled', (event) => {
     alert('Aplikasi sudah diinstal');
    });
     
     
    // if (window.matchMedia('(display-mode: standalone)').matches) {
    //   alert('display-mode is standalone');
    // }
  }

  add_to_home(){
    // debugger
    // hide our user interface that shows our button
    // Show the prompt
    this.deferredPrompt.prompt();
    // Wait for the user to respond to the prompt
    this.deferredPrompt.userChoice
      .then((choiceResult) => {
        if (choiceResult.outcome === 'accepted') {
          alert('User accepted the prompt');
        } else {
          alert('User dismissed the prompt');
        }
        this.deferredPrompt = null;
      });
  };

  loadPromo() {
    this.productService.getPromoSlider();

    this.events.subscribe('promoLoaded', () => {
      this.promoSliders = this.productService.promos;
      if(this.promoSliders.length>0){
        this.promoImagesLoaded =true;
      }
    })
  }

  loadProducts() {
    let loader = this.loadingCtrl.create({
      content: 'Memuat produk...'
    });
    loader.present();
    this.storage.get('token').then(token=>{
      if (token!==null){
        this.shopService.getPopProducts(token).then(data=>{
          let temp = data;
          if (temp){
            this.products = data.products;
            this.productRows = Array.from(Array(Math.ceil(this.products.length/2)).keys());
            this.products.forEach(product => {
              product.thumb=this.cfg.img + product.thumb;
            });
          } else {
            this.navCtrl.setRoot('WaitpagePage');
          };
          loader.dismiss();
        })
      }
    });
  }
  searchBtn(){
    this.isSearchBarOpened=true;
    setTimeout(() => {
      this.searchbar.setFocus();
    }, 100);
  }
  
  closeSearch(){
    this.isSearchBarOpened=false;
  }

  onSearch(event){
    this.navCtrl.setRoot("SearchPage",{keyword:event.target.value});
  }
  
  showDetails(product){
    let options: NativeTransitionOptions = {
      direction: 'up',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
     };
    this.nativePageTransitions.slide(options);
    this.navCtrl.push("SinglePage",{product:product});    
  }

}
