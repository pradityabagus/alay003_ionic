import { Component,ViewChild } from '@angular/core';
import { IonicPage, LoadingController, NavController, NavParams  } from 'ionic-angular';
import { ProtectedPage } from '../protected-page/protected-page';
import { ShopProvider } from '../../providers/commerce/shop';
import { AuthProvider } from '../../providers/auth/auth';
import { Storage } from '@ionic/storage';
import *  as AppConfig from '../../providers/config';

/**
 * Generated class for the AllProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-all-product',
  templateUrl: 'all-product.html',
})

export class AllProductPage extends ProtectedPage {
  
  public isSearchBarOpened = false;
  @ViewChild('searchbar') searchbar : any;
  page = 1;
  perPage = 20;
  totalData = 0;
  totalPage = 0;
  products:any[];
  productRows:any;
  cfg:any;
  showInf = true;
  constructor(public navCtrl: NavController, public storage:Storage, public navParams: NavParams,
     public shopService:ShopProvider, public loadingCtrl:LoadingController, public authService:AuthProvider) {
    super(navCtrl,navParams,storage,authService);
    this.cfg=AppConfig.cfg;
    this.getProducts();
  }

  ionViewDidLoad() {
    
  }

  getProducts(){
    let loader = this.loadingCtrl.create({
      content: 'Memuat produk...'
    });
    loader.present();
    this.storage.get('token').then(token=>{
      this.shopService.allProducts(token,this.perPage,this.page).then(data=>{
        this.products = data.data.products;
        this.totalPage= data.data.paginator.total_pages;
        this.productRows = Array.from(Array(Math.ceil(this.products.length/2)).keys());
        this.products.forEach(product => {
          product.thumb=this.cfg.img + product.thumb;
        });
        this.showInf= (this.page<this.totalPage)?true:false;
        loader.dismiss();
        this.totalData= data.data.paginator.total_count;
      },error=>{
        console.log(error);
      });
    })
  }

  doInfinite(infiniteScroll){
    if (this.totalData!=0){
      this.storage.get('token').then(token=>{
        this.page = this.page+1;
        setTimeout(() => {
          this.shopService.allProducts(token, this.perPage, this.page).then(data=>{
            this.totalData= data.data.paginator.total_count;
            this.totalPage= data.data.paginator.total_pages; 
            for (let i = 0; i < data.data.products.length; i++) {
              let prod = data.data.products[i];
              prod.thumb = this.cfg.img + prod.thumb;
              this.products.push(prod);
            };
            this.productRows = Array.from(Array(Math.ceil(this.products.length/2)).keys());
            if (this.page==this.totalPage){
              this.showInf=false;
            }
            infiniteScroll.complete();
          },error=>{
            console.log(error);
          });
          
        }, 100);
      });
    }
  }

  searchBtn(){
    this.isSearchBarOpened=true;
    setTimeout(() => {
      this.searchbar.setFocus();
    }, 100);
  }
  
  closeSearch(){
    this.isSearchBarOpened=false;
  }

  onSearch(event){
    this.navCtrl.setRoot("SearchPage",{keyword:event.target.value});
  }
  
  showDetails(product){
    this.navCtrl.push("SinglePage",{product:product});    
  }
}
