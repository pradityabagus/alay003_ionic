import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ShopProvider } from '../../providers/commerce/shop'
import { AuthProvider } from '../../providers/auth/auth';
import { CartProvider } from '../../providers/cart/cart';
import *  as AppConfig from '../../providers/config';
import { ImageViewerController } from 'ionic-img-viewer';
import { ProtectedPage } from '../protected-page/protected-page';


@IonicPage()
@Component({
  selector: 'page-single',
  templateUrl: 'single.html',
})
export class SinglePage extends ProtectedPage{
  selectProduct: any;
  productId: any;
  productCount: number = 0;
  stocks: any;
  cartItems: any[];
  cfg: any;
  // _imageViewerCtrl: ImageViewerController;

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
    private cartService: CartProvider, public toastCtrl: ToastController, public storage: Storage,
    public shopService: ShopProvider, public imageViewerCtrl: ImageViewerController, public authService:AuthProvider)
    {
      super(navCtrl,navParams,storage,authService);
    }

  ionViewDidEnter() {
    this.productId = this.navParams.get("product");
    this.cfg = AppConfig.cfg;
    this.getSingleProduct();

  }

  getSingleProduct() {
    console.log(this.productId);
    let loader = this.loadingCtrl.create({
      content: 'Memuat produk...'
    });
    this.storage.get('token').then(token => {
      if (token !== null) {
        this.shopService.getProduct(token, this.productId).then(data => {
          //console.log(data);
          if (data) {
            this.selectProduct = data.product;
            this.selectProduct.images = this.selectProduct.images.map(img => {
              return this.cfg.img + img;
            });
            this.stocks = this.selectProduct.stock.map(item => {
              return {
                variance: item.variance,
                stock: parseInt(item.stock),
                count: 0
              }
            })
          } else {
            this.navCtrl.setRoot('WaitpagePage');
          }
        });
      } else {
        this.navCtrl.setRoot('LoginPage');
      }
      loader.dismiss();
    });
  }

  ionViewDidLoad() {
    this.selectProduct = this.navParams.get("product");
    this.cartService.getCartItems().then((val) => {
      this.cartItems = val;
    })

  }

  decreaseProductCount(num) {
    if (this.stocks[num].count > 0) {
      this.stocks[num].count--;
    }
  }

  incrementProductCount(num) {
    if (this.stocks[num].stock > this.stocks[num].count) {
      this.stocks[num].count++;
    }
  }

  addToCart(product) {
    var items: Array<any> = new Array();
    this.stocks.map(stock => {
      this.productCount += stock.count;
      if (stock.count) {
        items.push({
          variance: stock.variance,
          count: stock.count,
        })
      }
    });
    var productPrice = this.productCount * parseInt(product.price);
    let cartProduct = {
      product_id: product.id,
      name: product.name,
      thumb: this.cfg.img + product.thumb,
      count: this.productCount,
      items: items,
      productPrice: parseInt(product.price),
      totalPrice: productPrice
    };
    console.log(cartProduct);
    if (this.productCount) {
      this.cartService.addToCart(cartProduct).then((val) => {
        this.presentToast(cartProduct.name);
      });
    } else {
      let toast = this.toastCtrl.create({
        message: `Masukkan jumlah kuantitas yang akan dibeli`,
        showCloseButton: true,
        duration: 3000,
        closeButtonText: 'Tutup'
      });
      toast.present();
    }
  }

  presentToast(name) {
    let toast = this.toastCtrl.create({
      message: `${name} ditambahkan ke keranjang`,
      duration: 1000
    });
    this.navCtrl.push('CartPage');
    toast.present();
  }

  onClick(imageToView) {
    const viewer = this.imageViewerCtrl.create(imageToView)
    viewer.present();
  }

}
