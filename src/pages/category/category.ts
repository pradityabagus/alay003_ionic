import { Component,ViewChild } from '@angular/core';
import { IonicPage, LoadingController, NavController, NavParams, Events } from 'ionic-angular';
import { ShopProvider } from '../../providers/commerce/shop';
import {Storage} from '@ionic/storage';
import *  as AppConfig from '../../providers/config';
import { AuthProvider } from '../../providers/auth/auth';
import { ProtectedPage } from '../protected-page/protected-page';
@IonicPage()
@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
})
export class CategoryPage extends ProtectedPage {
  public isSearchBarOpened = false;
  @ViewChild('searchbar') searchbar : any;
  products: any[];
  productRows:any;
  category:any;
  categories:any[];
  title:string;
  cfg:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public loadingCtrl: LoadingController,public storage:Storage,
    private shopService: ShopProvider, private events: Events, public authService:AuthProvider) {
      super(navCtrl,navParams,storage,authService);
      this.category = navParams.get('category');
      this.cfg=AppConfig.cfg;
  }

  ionViewDidLoad() {
    let loader = this.loadingCtrl.create({
      content: 'Memuat produk...'
    });
    loader.present();
    this.storage.get('token').then(token=>{
      if (token!==null){
        this.shopService.getCatProducts(token,this.category).then(data=>{
          //console.log(data);
          if (data){
            this.title = data.category;
            this.categories = data.childCategory;
            this.products= data.products;
            this.productRows = Array.from(Array(Math.ceil(this.products.length/2)).keys());
            this.products.forEach(product => {
              product.thumb=this.cfg.img + product.thumb;
            });
            this.categories.forEach(cat => {
              cat.thumbKategori=this.cfg.img + cat.thumbKategori;
            });
            loader.dismiss();
          } else {
            this.navCtrl.setRoot('WaitpagePage');
          }
        });
      } else {
        this.navCtrl.setRoot('LoginPage');
      }
      
    });
    
  }

  searchBtn(){
    this.isSearchBarOpened=true;
    setTimeout(() => {
      this.searchbar.setFocus();
    }, 100);
  }
  
  closeSearch(){
    this.isSearchBarOpened=false;
  }

  onSearch(event){
    this.navCtrl.setRoot("SearchPage",{keyword:event.target.value});
  }

  goToCategory(category){
    this.navCtrl.push('CategoryPage',{category:category})
 }
  
  showDetails(product){
    this.navCtrl.push("SinglePage",{product:product});    
  }

}
