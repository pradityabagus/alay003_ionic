import {NavController, NavParams} from 'ionic-angular';
import {AuthProvider} from '../../providers/auth/auth';
import {Storage} from '@ionic/storage';

export class ProtectedPage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public storage: Storage,
    public authService: AuthProvider,
  ) {
  }

  ionViewCanEnter() {

    this.storage.get('token').then(token => {
      if (token === null) {
        this.navCtrl.setRoot('LoginPage');
        return false;
      } else {
        this.authService.activeUser(token).then(trus=>{
          if (trus==0 || trus===null){
            this.navCtrl.setRoot('WaitpagePage');
            return false;
          };
          if (trus==266){
            this.authService.logout();
            this.navCtrl.setRoot('LoginPage');
            return false;
          }
        }, error=>{
          this.authService.logout();
          this.navCtrl.setRoot('LoginPage');
        })
      }
    });
    return true;
  }
}
