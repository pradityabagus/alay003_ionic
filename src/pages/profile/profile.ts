import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ProtectedPage } from '../protected-page/protected-page';
import { AuthProvider } from '../../providers/auth/auth';
// import { database } from '../../../node_modules/firebase';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage extends ProtectedPage{
  user_name:string;
  user_phone:string;
  user_email:string;
  user_address:string;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage:Storage, public authService:AuthProvider,
    private loadingCtrl:LoadingController, public alertCtrl:AlertController,
  ) {
    super(navCtrl,navParams,storage,authService);
  }

  ionViewDidEnter() {
    let loader = this.loadingCtrl.create({
      content: 'Mengambil Data..'
    });
    loader.present();
    this.storage.get('phone').then(phone => {
      this.user_phone=phone;
      this.storage.get('fullname').then(name => {
        this.user_name = name;
      });
      this.storage.get('email').then(email => {
        this.user_email = email;
      });
      this.storage.get('address').then(address => {
        this.user_address = address;
      });
      loader.dismiss();
    })
    
  }
  toChangePass(){
    this.navCtrl.push('ChangePassPage')
  }

  saveProfile(){
    let loader = this.loadingCtrl.create({
      content: 'Mengambil Data..'
    });
    loader.present();
    var profileData ={
      name : this.user_name,
      email : this.user_email,
      address : this.user_address
    }
    this.storage.get('token').then(token=>{
      this.authService.updateProfile(profileData,token).then(resp=>{
        var data = resp;
        this.presentAlert(data.message)
        if (data.status_code=200){
          this.storage.set('fullname',data.profile.name);
          this.storage.set('email',data.profile.email);
          this.storage.set('address',data.profile.address);
        }
        if (resp.status==200){
          this.authService.logout();
          this.navCtrl.setRoot('LoginPage');
        };
        loader.dismiss();
      }, error=>{
        loader.dismiss();
      })
    })
  }
  presentAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'Luck Mas',
      subTitle: message,
      buttons: ['Close']
    });
    alert.present();
  }
}
