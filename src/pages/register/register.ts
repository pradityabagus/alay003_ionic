// import { Component } from "@angular/core";
// import { IonicPage, NavController } from "ionic-angular";
// import { AuthProvider } from "../../providers/auth/auth";


// @IonicPage()
// @Component({
//   selector: "page-register",
//   templateUrl: "register.html"
// })
// export class RegisterPage {
//   name: any;
//   address: any;
//   email: any;
//   password: any;
//   constructor(
//     public navCtrl: NavController,
//     public AuthService: AuthProvider
//   ) {}

//   ionViewDidLoad() {}

//   register() {
//     var userObj = {
//       name: this.name,
//       address: this.address,
//       email: this.email,
//       password: this.password
//     };

//     this.AuthService.register(userObj)
//       .then((response: any) => {
//         if (response.success == true) {
//           this.navCtrl.push('CheckoutPage');
//         }
//       })
//       .catch(err => {
//         alert(JSON.stringify(err));
//       });
//   }

//   showLoginPage() {
//     this.navCtrl.push("LoginPage");
//   }
// }
import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, MenuController, LoadingController, AlertController} from 'ionic-angular';
import {Storage} from '@ionic/storage';
// import {Validators, FormBuilder, FormGroup} from '@angular/forms';
import {AuthProvider} from '../../providers/auth/auth';


@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {

    name: any;
    phone:any;
    address:any;
    password:any;
    password_confirmation:any;

  constructor(
    private loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public storage: Storage,
    private alertCtrl: AlertController,
    // public formBuilder: FormBuilder,
    public authService: AuthProvider) 
    {
      this.menuCtrl.enable(false, 'menuMain');
      
    // this.regData = this.formBuilder.group({
    //   name: ['', Validators.required],
    //   email: ['', Validators.required],
    //   phone: ['', Validators.required]
    //   password: ['', Validators.required],
    //   confirm_password: ['', Validators.required]
    // });

    }

  ionViewDidLoad() {
    //hide menu when on the login page, regardless of the screen resolution
    // this.menuCtrl.enable(false);
  }

  register() {
    let loader = this.loadingCtrl.create({
      content: 'Memproses Data..'
    });
    loader.present();
    let regData = {
      name:this.name,
      phone:this.phone,
      address:this.address,
      password:this.password,
      password_confirmation:this.password_confirmation
    }
    // this.authService.register(regData)
    //   .then(() => console.log('this way')/* this.navCtrl.setRoot('ProfilePage') */)
    //   .catch(e => console.log("reg error", e));
    let errore:any;
      this.authService.register(regData).then((data)=>{ 
        errore=data;
        console.log(data);
        loader.dismiss();
        if (errore.status =='error'){
          this.presentAlert(errore.message + JSON.stringify(errore.data));
        } else {
          this.presentAlert("Pendaftaran berhasil...");
          this.menuCtrl.enable(true, 'menuMain');
          this.navCtrl.setRoot('HomePage');
        }
        
      })
  }

  showLoginPage() {
    this.navCtrl.setRoot("LoginPage");
  }
  presentAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'Luck Mas',
      subTitle: message,
      buttons: ['Close']
    });
    alert.present();
  }
}
